<?php
/**
 * Copyright since 2023 Jiri Antonu and CZ.NIC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/license/gpl-3-0/
 *
 * @author    Jiri Antonu and CZ.NIC <jiri.antonu@nic.cz>
 * @copyright Since 2023 Jiri Antonu and CZ.NIC
 * @license   https://opensource.org/license/gpl-3-0/ GNU General Public License version 3.0
 */
require(realpath(dirname(__FILE__) . '/utils/redirecturl.php'));
require(realpath(dirname(__FILE__) . '/utils/randomstring.php'));

if (!defined('_PS_VERSION_')) {
    exit;
}

class OpenIDConnect extends Module
{
    public $logo_path;

    public function __construct()
    {
        $this->name = 'openidconnect';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Jiří Antoňů | CZ.NIC';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.7.0.0',
            'max' => '8.99.99',
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('OpenID Connect');
        $this->description = $this->l('This is an OpenID Connect authentication module.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('OPENID_ID') || !Configuration::get('OPENID_SECRET')) {
            $this->warning = $this->l('No credentials provided.');
        }

        if (!Configuration::get('OPENID_AUTHORIZATION_URL') || !Configuration::get('OPENID_TOKEN_URL') || !Configuration::get('OPENID_USERINFO_URL')) {
            $this->warning = $this->l('No URLs provided.');
        }

        if (!Configuration::get('OPENID_SCOPE')) {
            $this->warning = $this->l('No scope provided.');
        }

        if (!Configuration::get('OPENID_REQUIRE_PROMPT')) {
            $this->warning = $this->l('Require prompt setting not set.');
        }

        $this->logo_path = $this->_path . 'logo.png';
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        return (parent::install()
            && $this->registerHook('displayNav2')
        );
    }

    public function uninstall()
    {
        return (parent::uninstall()
            && Configuration::deleteByName('OPENID_IDP_NAME')
            && Configuration::deleteByName('OPENID_ID')
            && Configuration::deleteByName('OPENID_SECRET')
            && Configuration::deleteByName('OPENID_AUTHORIZATION_URL')
            && Configuration::deleteByName('OPENID_TOKEN_URL')
            && Configuration::deleteByName('OPENID_USERINFO_URL')
            && Configuration::deleteByName('OPENID_SCOPE')
            && Configuration::deleteByName('OPENID_REQUIRE_PROMPT')
        );
    }

    /**
     * This method handles the module's configuration page
     * @return string The page's HTML content 
     */
    public function getContent()
    {
        $output = '';

        // this part is executed only when the form is submitted
        if (Tools::isSubmit('submit' . $this->name)) {
            // retrieve the value set by the user
            $idp_name = (string) Tools::getValue('OPENID_IDP_NAME');
            $id = (string) Tools::getValue('OPENID_ID');
            $secret = (string) Tools::getValue('OPENID_SECRET');
            $authorization_url = (string) Tools::getValue('OPENID_AUTHORIZATION_URL');
            $token_url = (string) Tools::getValue('OPENID_TOKEN_URL');
            $userinfo_url = (string) Tools::getValue('OPENID_USERINFO_URL');
            $scope = (string) Tools::getValue('OPENID_SCOPE');
            $require_prompt = (string) Tools::getValue('OPENID_REQUIRE_PROMPT');

            // check that the value is valid
            if (empty($id) || empty($secret) || empty($authorization_url) || empty($token_url) || empty($userinfo_url) || empty($scope)) {
                // invalid value, show an error
                $output = $this->displayError($this->l('Provide all values.'));
            } else {
                // value is ok, update it and display a confirmation message
                Configuration::updateValue('OPENID_IDP_NAME', $idp_name);
                Configuration::updateValue('OPENID_ID', $id);
                Configuration::updateValue('OPENID_SECRET', $secret);
                Configuration::updateValue('OPENID_AUTHORIZATION_URL', $authorization_url);
                Configuration::updateValue('OPENID_TOKEN_URL', $token_url);
                Configuration::updateValue('OPENID_USERINFO_URL', $userinfo_url);
                Configuration::updateValue('OPENID_SCOPE', $scope);
                Configuration::updateValue('OPENID_REQUIRE_PROMPT', $require_prompt);
                $output = $this->displayConfirmation($this->l('Settings updated'));
            }
        }

        // display any message, then the form
        return $output . $this->displayForm();
    }

    /**
     * Builds the configuration form
     * @return string HTML code
     */
    public function displayForm()
    {
        // Init Fields form array
        $form = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Settings'),
                ],
                'input' => [
                    [
                        'type' => 'text',
                        'label' => $this->l('IdP name (title)'),
                        'name' => 'OPENID_IDP_NAME',
                        'size' => 20,
                        'required' => false,
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Client ID'),
                        'name' => 'OPENID_ID',
                        'size' => 20,
                        'required' => true,
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Client secret'),
                        'name' => 'OPENID_SECRET',
                        'size' => 20,
                        'required' => true,
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Authorization endpoint URL'),
                        'name' => 'OPENID_AUTHORIZATION_URL',
                        'size' => 20,
                        'required' => true,
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Token endpoint URL'),
                        'name' => 'OPENID_TOKEN_URL',
                        'size' => 20,
                        'required' => true,
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Userinfo endpoint URL'),
                        'name' => 'OPENID_USERINFO_URL',
                        'size' => 20,
                        'required' => true,
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Scope'),
                        'name' => 'OPENID_SCOPE',
                        'size' => 20,
                        'required' => true,
                    ],
                    [
                        'type' => 'checkbox',
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'PROMPT',
                                    'name' => $this->l('Require prompt'),
                                    'val' => '1',
                                    'checked' => 'checked'
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'label' => $this->l('Require prompt'),
                        'name' => 'OPENID_REQUIRE',
                        'size' => 20,
                        'required' => true,
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Redirect URL'),
                        'name' => 'OPENID_REDIRECT_URL',
                        'size' => 20,
                        'disabled' => true,
                    ],
                ],
                'submit' => [
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right',
                ],
            ],
        ];

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->table = $this->table;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&' . http_build_query(['configure' => $this->name]);
        $helper->submit_action = 'submit' . $this->name;

        // Default language
        $helper->default_form_language = (int) Configuration::get('PS_LANG_DEFAULT');

        // Load current value into the form
        $helper->fields_value['OPENID_IDP_NAME'] = Tools::getValue('OPENID_IDP_NAME', Configuration::get('OPENID_IDP_NAME'));
        $helper->fields_value['OPENID_ID'] = Tools::getValue('OPENID_ID', Configuration::get('OPENID_ID'));
        $helper->fields_value['OPENID_SECRET'] = Tools::getValue('OPENID_SECRET', Configuration::get('OPENID_SECRET'));
        $helper->fields_value['OPENID_AUTHORIZATION_URL'] = Tools::getValue('OPENID_AUTHORIZATION_URL', Configuration::get('OPENID_AUTHORIZATION_URL'));
        $helper->fields_value['OPENID_TOKEN_URL'] = Tools::getValue('OPENID_TOKEN_URL', Configuration::get('OPENID_TOKEN_URL'));
        $helper->fields_value['OPENID_USERINFO_URL'] = Tools::getValue('OPENID_USERINFO_URL', Configuration::get('OPENID_USERINFO_URL'));
        $helper->fields_value['OPENID_SCOPE'] = Tools::getValue('OPENID_SCOPE', Configuration::get('OPENID_SCOPE'));
        $helper->fields_value['OPENID_REQUIRE_PROMPT'] = Tools::getValue('OPENID_REQUIRE_PROMPT', Configuration::get('OPENID_REQUIRE_PROMPT'));
        $helper->fields_value['OPENID_REDIRECT_URL'] = getRedirectUrl();

        return $helper->generateForm([$form]);
    }

    public function hookDisplayNav2($params)
    {
        $this->context->smarty->assign([
            'login_link' => $this->context->link->getModuleLink('openidconnect', 'login'),
            'authenticated' => ($this->context->customer && $this->context->customer->id),
            'title' => Configuration::get("OPENID_IDP_NAME") ?: 'OpenID'
        ]);

        return $this->display(__FILE__, 'login.tpl');
    }
}
