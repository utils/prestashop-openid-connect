<?php
/**
 * Copyright since 2023 Jiri Antonu and CZ.NIC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/license/gpl-3-0/
 *
 * @author    Jiri Antonu and CZ.NIC <jiri.antonu@nic.cz>
 * @copyright Since 2023 Jiri Antonu and CZ.NIC
 * @license   https://opensource.org/license/gpl-3-0/ GNU General Public License version 3.0
 */
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
header('Location: ../');
exit;
