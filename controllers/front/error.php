<?php
/**
 * Copyright since 2023 Jiri Antonu and CZ.NIC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/license/gpl-3-0/
 *
 * @author    Jiri Antonu and CZ.NIC <jiri.antonu@nic.cz>
 * @copyright Since 2023 Jiri Antonu and CZ.NIC
 * @license   https://opensource.org/license/gpl-3-0/ GNU General Public License version 3.0
 */
class openidconnecterrorModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $this->context->smarty->assign([
            'error_message' => Tools::getValue('error')
        ]);
        $this->setTemplate('module:openidconnect/views/templates/front/error.tpl');
    }
}
