<?php

/**
 * Copyright since 2023 Jiri Antonu and CZ.NIC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/license/gpl-3-0/
 *
 * @author    Jiri Antonu and CZ.NIC <jiri.antonu@nic.cz>
 * @copyright Since 2023 Jiri Antonu and CZ.NIC
 * @license   https://opensource.org/license/gpl-3-0/ GNU General Public License version 3.0
 */
class openidconnectcallbackModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $error = Tools::getValue('error');
        if ($error) Tools::redirect($this->context->link->getModuleLink('openidconnect', 'error', array('error' => $error)));

        $client = new \GuzzleHttp\Client();

        $token_response = $client->request('POST', Configuration::get('OPENID_TOKEN_URL'), [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'code' => Tools::getValue('code'),
                'redirect_uri' => getRedirectUrl()
            ],
            'auth' => [Configuration::get('OPENID_ID'), Configuration::get('OPENID_SECRET')]
        ]);
        $token_body = json_decode($token_response->getBody());
        $token = $token_body->access_token;

        if (empty($token)) Tools::redirect($this->context->link->getModuleLink('openidconnect', 'error', array('error' => 'Authentication failed at token request.')));

        $data_response = $client->request('GET', Configuration::get('OPENID_USERINFO_URL'), [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ]
        ]);
        $data = json_decode($data_response->getBody());

        if (empty($data->email)) Tools::redirect($this->context->link->getModuleLink('openidconnect', 'error', array('error' => 'Authentication failed at userinfo request.')));

        Hook::exec('actionAuthenticationBefore');

        $existing_customer = new Customer();
        $authentication = $existing_customer->getByEmail($data->email);

        if (isset($authentication->active) && !$authentication->active) {
            Tools::redirect($this->context->link->getModuleLink('openidconnect', 'error'));
        } elseif (!$authentication || !$existing_customer->id || $existing_customer->is_guest) {
            $new_customer = new Customer();

            $new_customer->email = $data->email;
            $new_customer->firstname = $data->given_name ?: "User";
            $new_customer->lastname = $data->family_name ?: "User";
            $new_customer->setWsPasswd(generateRandomString());
            $new_customer->add();

            $this->context->updateCustomer($new_customer);
        } else {
            $this->context->updateCustomer($existing_customer);
        }

        Hook::exec('actionAuthentication', ['customer' => $this->context->customer]);
        Tools::redirect('/index.php');
    }
}
