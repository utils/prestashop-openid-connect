<?php
/**
 * Copyright since 2023 Jiri Antonu and CZ.NIC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/license/gpl-3-0/
 *
 * @author    Jiri Antonu and CZ.NIC <jiri.antonu@nic.cz>
 * @copyright Since 2023 Jiri Antonu and CZ.NIC
 * @license   https://opensource.org/license/gpl-3-0/ GNU General Public License version 3.0
 */
class openidconnectloginModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $state =
            Tools::redirect(
                Configuration::get('OPENID_AUTHORIZATION_URL')
                    . '?response_type=code&scope='
                    . Configuration::get('OPENID_SCOPE')
                    . '&client_id='
                    . Configuration::get('OPENID_ID')
                    . '&state=' . generateRandomString()
                    . '&redirect_uri='
                    . urlencode(getRedirectUrl())
                    . (Configuration::get('OPENID_REQUIRE_PROMPT') ? '&prompt=consent' : '')
            );
    }
}
