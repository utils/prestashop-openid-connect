{**
 * Copyright since 2023 Jiri Antonu and CZ.NIC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU General Public License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/license/gpl-3-0/
 *
 * @author    Jiri Antonu and CZ.NIC <jiri.antonu@nic.cz>
 * @copyright Since 2023 Jiri Antonu and CZ.NIC
 * @license   https://opensource.org/license/gpl-3-0/ GNU General Public License version 3.0
 *}

<!-- Block mymodule -->
{if !$authenticated}
  <div id="openidconnect_login_block">
    <div class="openidconnect-login user-info">
            <a href="{$login_link}" title="Log in to your customer account" rel="nofollow">
          <i class="material-icons"></i>
          <span class="hidden-sm-down">Sign in via {$title}</span>
        </a>
        </div>
  </div>
{/if}
<!-- /Block mymodule -->
